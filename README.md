

deploy:

    ansible-galaxy install -f -r requirements.yml -p .galaxy
    ansible-playbook -i fdroidSearch, webserver.yml
    ansible-playbook -i fdroidSearch, searchcontainer.yml
